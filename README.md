# Build docker image

`docker-compose build`

# Start Containers

`docker-compose up -d`

# Make migrations

`docker-compose exec web bash`
`python manage.py migrate`

# Start a mysql db with docker

`docker run --name animap-db -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=animap -d mysql:latest`

# Ngrok
ngrok http 8080