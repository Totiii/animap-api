import datetime
import os
from urllib import request
from django.core.files import File
from django.forms import model_to_dict
from rest_framework import serializers
from api.models import User
from django.contrib.auth.hashers import make_password


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=255, min_length=2)
    email = serializers.EmailField(max_length=255, min_length=4)
    first_name = serializers.CharField(max_length=255, min_length=2)
    last_name = serializers.CharField(max_length=255, min_length=2)
    password = serializers.CharField(max_length=65, min_length=8, write_only=True, style={'input_type': 'password'})
    phone_number = serializers.CharField(max_length=20, min_length=8)
    push_token = serializers.CharField(max_length=50, min_length=8)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password', 'phone_number', 'push_token']


    def validate(self, attrs):
        email = attrs.get('email', '')
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                {'email': 'Email is already in use'})
        username = attrs.get('username', '')
        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError(
                {'username': 'Username is already in use'})
        return super().validate(attrs)

    def create(self, validated_data):
        new_user = User.objects.create(
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            email=validated_data['email'],
            password=make_password(validated_data['password']),
            phone_number=validated_data['phone_number'],
            push_token=validated_data['push_token']
        )
        result = request.urlretrieve("https://identicon-api.herokuapp.com/"+validated_data['username']+"/500>?format=png")
        new_user.profile_pic.save(
            validated_data['username'] + ".png",
            File(open(result[0], 'rb'))
        )

        new_user.save()
        return model_to_dict(new_user)