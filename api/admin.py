from django.contrib import admin
from django.apps import apps
from django.contrib.auth.models import Group, User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
models = apps.get_models()


class UserAdmin(BaseUserAdmin):
    search_fields = ['username', 'first_name', 'last_name']
    list_display = ('username', 'first_name', 'last_name', 'is_active')
    filter_horizontal = ('groups', 'user_permissions')
    fieldsets = (
        ('Informations de connexion', {'fields': ('username', 'password')}),
        ('Informations personnelles', {'fields': (
            'first_name',
            'last_name',
            'email',
            'profile_pic',
            'phone_number'
        )}),
        ('Permissions', {'fields': (
            'is_active',
            'is_staff',
            'is_superuser',
            'groups',
            'user_permissions'
        )}),
    )


admin.site.register(User, UserAdmin)
for model in models:
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass