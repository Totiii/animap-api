from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from api import views

urlpatterns = [
    path('search', views.SearchAPI.as_view(), name='search'),
    path('search/<int:search_id>', views.ASearchAPI.as_view(), name='a_search'),
    path('register', views.RegisterView.as_view(), name='register'),
    path('user', views.UserView.as_view(), name='user'),
    path('post', views.PostAPI.as_view(), name='post'),
    path('post/<int:post_id>', views.APostAPI.as_view(), name='a_post'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]