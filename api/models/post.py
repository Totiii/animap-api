import datetime
from urllib import request

from django.db.models.signals import post_save
from django.urls import reverse

from animap.settings import ANIMALS
from .user import User
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True, default=None)
    animal = models.CharField(max_length=300, choices=ANIMALS)
    location = models.PointField(geography=True, default=Point(0.0, 0.0))
    image = models.ImageField(upload_to="posts/")
    date = models.DateTimeField(null=False, default=datetime.datetime.now)


def search_relative_search(sender, **kwargs):
    from ..models.search import Search
    from ..views import send_push_message
    located_search = []
    post = Post.objects.get(id=kwargs["instance"].id)
    relative_search = Search.objects.filter(animal=post.animal,
                                            location__distance_lte=(kwargs["instance"].location, 500 / 111))
    for query_search in relative_search:
        located_search = relative_search.filter(
            location__distance_lte=(kwargs["instance"].location, query_search.range / 111))

    for search in located_search:
        send_push_message(search.user,
                          "Animal trouvé | Animap",
                          "Un utilisateur à trouvé un animal qui correspond à une de vos recherche.",
                          {"search_info": {
                              "api_url": "",
                              "avatar": post.image.url if post.image else None,
                              "coordinate": {
                                  "latitude": post.location.x,
                                  "longitude": post.location.y,
                              },
                              "description": post.description,
                              "user":
                                  {
                                      "fullname": post.user.get_full_name(),
                                      "id": post.user.id,
                                      "mail": post.user.email,
                                      "phone_number": post.user.phone_number,
                                  }
                          },
                              "type": "search_found"}
                          )


post_save.connect(search_relative_search, sender=Post)
