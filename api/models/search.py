import datetime

from animap.settings import ANIMALS
from .user import User
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point


class Search(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    animal = models.CharField(max_length=300, choices=ANIMALS)
    location = models.PointField(geography=True, default=Point(0.0, 0.0))
    range = models.IntegerField(default=5, null=True, blank=False)
    date = models.DateTimeField(default=datetime.datetime.now, null=False, blank=False)