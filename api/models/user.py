import os
from uuid import uuid4
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models


def path_and_rename(instance, filename):
    upload_to = 'profile_images'
    ext = filename.split('.')[-1]
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)


class User(AbstractUser):
    profile_pic = models.ImageField(upload_to=path_and_rename, blank=True, null=True)
    phone_number = models.CharField(null=False, blank=False, max_length=20)
    push_token = models.CharField(null=True, blank=False, max_length=50)

