# Generated by Django 3.2.9 on 2021-11-18 14:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_search_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='search',
            name='description',
        ),
    ]
