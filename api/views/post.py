import datetime

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.urls import reverse
from rest_framework import status, serializers
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from django.contrib.gis.geos import Point

from animap.settings import ANIMALS
from api.models import Post, User


class PostAPI(GenericAPIView):

    def get(self, request):
        posts = Post.objects.all()
        # add optional request params for search
        req_user = request.user
        req_description = request.GET.get("description", None)
        req_date = request.GET.get("date", None)
        req_location_x = request.GET.get("location_x", None)
        req_location_y = request.GET.get("location_y", None)
        req_range = request.GET.get("location_range", 15)
        if req_user and req_user in User.objects.all():
            posts = posts.filter(user__id=req_user.id)
        if req_description:
            posts = posts.filter(description__icontains=str(req_description))
        if req_date:
            posts = posts.filter(date=datetime.datetime.strptime(req_date, '%d/%m/%Y %H:%M:%S'))
        if req_location_x and req_location_y and req_range:
            posts = posts.filter(location__distance_lte=(
                Point(float(req_location_x), float(req_location_y)), int(req_range) / 111))
        page = request.GET.get('page', 1)

        posts_paginate = Paginator(posts, 20)
        try:
            page = int(page)
            page_posts = posts_paginate.page(page)
        except PageNotAnInteger:
            page = posts_paginate.page_range[0]
            page_posts = posts_paginate.page(page)
        except EmptyPage:
            page = posts_paginate.page_range[0]
            page_posts = posts_paginate.page(page)
        post_list = []
        for post in page_posts:
            post_list.append({
                "api_url": request.build_absolute_uri(reverse('a_post', args=(post.id,))),
                "id": post.id,
                "description": post.description,
                "animal": post.animal,
                "image": post.image.url if post.image else None,
                'location': [post.location.x, post.location.y],
                "datetime": post.date.strftime('%d/%m/%Y %H:%M:%S'),
                "user": {"fullname": post.user.get_full_name(), "phone_number": post.user.phone_number, "mail": post.user.email, "id": post.user.id}
            })
        response = {"nb_posts": posts.count(), "actual_page": page, "nb_pages": posts_paginate.num_pages, "posts": post_list}

        return Response(response, status=status.HTTP_200_OK)

    def post(self, request):

        user = request.user
        # required POST
        req_description = str(request.POST.get("description", None))
        req_animal = str(request.POST.get("animal", None))
        req_location_x = request.POST.get("location_x", None)
        req_location_y = request.POST.get("location_y", None)

        # none require POST

        image = request.FILES.get("image", None)

        if not (req_description or req_animal or req_location_x or req_location_y):
            return Response("missing_field", status=status.HTTP_406_NOT_ACCEPTABLE)

        if req_animal not in dict(ANIMALS):
            return Response("animal_not_found", status=status.HTTP_404_NOT_FOUND)

        post = Post(user=user, description=req_description, animal=req_animal,
                    location=Point(float(req_location_x), float(req_location_y)))
        if image:
            post.image = image
        post.save()
        response = {
            "api_url": request.build_absolute_uri(reverse('a_post', args=(post.id,))),
            "id": post.id,
            "description": post.description,
            "animal": post.animal,
            "image": post.image.url if post.image else None,
            'location': [post.location.x, post.location.y],
            "datetime": post.date.strftime('%d/%m/%Y %H:%M:%S'),
            "user": {"fullname": post.user.get_full_name(), "phone_number": post.user.phone_number,
                     "mail": post.user.email, "id": post.user.id}
        }
        return Response(response, status=status.HTTP_201_CREATED)

    def delete(self, request):
        id = request.POST.get("id", None)
        if not id:
            return Response("missing_field", status=status.HTTP_406_NOT_ACCEPTABLE)
        try:
            Post.objects.get(id=int(id)).delete()
        except Post.DoesNotExist:
            return Response("post_not_found", status=status.HTTP_404_NOT_FOUND)

        return Response(None, status=status.HTTP_200_OK)



class APostAPI(GenericAPIView):
    def get(self, request, post_id):
        try:
            post = Post.objects.get(id=int(post_id))
        except Post.DoesNotExist:
            return Response("post_not_found", status=status.HTTP_404_NOT_FOUND)

        response = {
            "api_url": request.build_absolute_uri(reverse('a_post', args=(post.id,))),
            "id": post.id,
            "description": post.description,
            "animal": post.animal,
            "image": post.image.url if post.image else None,
            'location': [post.location.x, post.location.y],
            "datetime": post.date.strftime('%d/%m/%Y %H:%M:%S'),
            "user": {"fullname": post.user.get_full_name(), "phone_number": post.user.phone_number,
                     "mail": post.user.email, "id": post.user.id}
        }


        return Response(response, status=status.HTTP_200_OK)
