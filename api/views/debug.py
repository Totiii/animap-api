from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from animap.settings import pusher_client
from api.models import Post
from api.views import send_push_message



@csrf_exempt
def debug(request):
    print(request.POST)
    if request.POST.get("push", None):
        post = Post.objects.filter().first()
        send_push_message(request.user,
                          "Animal trouvé | Animap",
                          "Un utilisateur à trouvé un animal qui correspond à une de vos recherches.",
                          {"search_info": {
                              "api_url": "",
                              "avatar": post.image.url if post.image else None,
                              "coordinate": {
                                  "latitude": post.location.x,
                                  "longitude": post.location.y,
                              },
                              "description": post.description,
                              "user":
                                  {
                                      "fullname": post.user.get_full_name(),
                                      "id": post.user.id,
                                      "mail": post.user.email,
                                      "phone_number": post.user.phone_number,
                                  }
                          },
                              "type": "search_found"}
                          )

        messages.success(request, "OK push")
    return render(request, 'debug.html', locals())
