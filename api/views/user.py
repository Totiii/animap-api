from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from api.serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status


class RegisterView(GenericAPIView):
    serializer_class = UserSerializer

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {
                    'user': serializer.data,
                    'message': 'User created'
                },
                status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserView(GenericAPIView):
    serializer_class = UserSerializer

    def get(self, request):
        user = request.user
        permission_classes = [IsAuthenticated]

        return Response({
            "first_name": user.first_name,
            "last_name": user.last_name,
            "avatar": user.profile_pic.url if user.profile_pic else None,
            "phone": user.phone_number,
            "email": user.email,
            "id": user.id,
            "username": user.username,
            "push_token": user.push_token,
        }, status=status.HTTP_200_OK)

    def patch(self, request):
        user = request.user
        permission_classes = [IsAuthenticated]

        first_name = str(request.POST.get("first_name", None))
        last_name = str(request.POST.get("last_name", None))
        phone = str(request.POST.get("phone", None))

        if not (first_name or last_name or phone):
            return Response("missing_field", status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            user.first_name = first_name
            user.last_name = last_name
            user.phone_number = phone
            user.save()

        return Response({
            "first_name": user.first_name,
            "last_name": user.last_name,
            "avatar": user.profile_pic.url if user.profile_pic else None,
            "phone": user.phone_number,
            "email": user.email,
        }, status=status.HTTP_201_CREATED)
