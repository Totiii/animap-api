from exponent_server_sdk import (
    DeviceNotRegisteredError,
    PushClient,
    PushMessage,
    PushServerError,
    PushTicketError,
)
from requests.exceptions import ConnectionError, HTTPError
from animap.settings import pusher_client


def send_push_message(user, title, message, extra=None):
    try:
        response = PushClient().publish(
            PushMessage(to=user.push_token,
                        title=title,
                        body=message,
                        data=extra))
    except PushServerError as exc:
        # Encountered some likely formatting/validation error.
        print("puch error")
        print(exc)
        raise
    except (ConnectionError, HTTPError) as exc:
        # Encountered some Connection or HTTP error - retry a few times in
        # case it is transient.
        print("connextion error")
        print(exc)
        raise

    try:
        # We got a response back, but we don't know whether it's an error yet.
        # This call raises errors so we can handle them with normal exception
        # flows.
        response.validate_response()
    except DeviceNotRegisteredError:
        # Mark the push token as inactive
        print("device nt register")
    except PushTicketError as exc:
        # Encountered some other per-notification error.
        print("PushTicketError")
        print(exc)

    pusher_client.trigger(user.username, 'search_found', extra)
