import datetime

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.urls import reverse
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.contrib.gis.geos import Point

from animap.settings import ANIMALS
from api.models import Search, Post, User


class SearchAPI(GenericAPIView):

    #permission_classes = [IsAuthenticated]
    def get(self, request):

        searchs = Search.objects.filter(user=request.user)
        req_animal = request.GET.get("animal", None)
        req_date = request.GET.get("date", None)
        req_location_x = request.GET.get("location_x", None)
        req_location_y = request.GET.get("location_y", None)
        if req_animal:
            searchs = searchs.filter(animal=str(req_animal))
        if req_date:
            searchs = searchs.filter(date=datetime.datetime.strptime(req_date, '%d/%m/%Y %H:%M:%S'))
        if req_location_x and req_location_y:
            searchs = searchs.filter(location__distance_lte=(
                Point(float(req_location_x), float(req_location_y))))
        page = request.GET.get('page', 1)

        searchs_paginate = Paginator(searchs, 20)
        try:
            page = int(page)
            page_searchs = searchs_paginate.page(page)
        except PageNotAnInteger:
            page = searchs_paginate.page_range[0]
            page_searchs = searchs_paginate.page(page)
        except EmptyPage:
            page = searchs_paginate.page_range[0]
            page_searchs = searchs_paginate.page(page)
        search_list = []
        for search in page_searchs:
            search_list.append({
            "api_url": request.build_absolute_uri(reverse('a_search', args=(search.id,))),
            "id": search.id,
            "animal": search.animal,
            "user": {"username": search.user.username, "user_id": search.user.id},
            'location': [search.location.x, search.location.y],
            'range': search.range,
            "datetime": search.date.strftime('%d/%m/%Y %H:%M:%S')
        })


        response = {"nb_searchs": searchs.count(), "actual_page": page, "nb_pages": searchs_paginate.num_pages,
                    "searchs": search_list}

        return Response(response, status=status.HTTP_200_OK)

    def post(self, request):
        user = request.user

        # required POST
        req_animal = str(request.POST.get("animal", None))
        req_location_x = request.POST.get("location_x", None)
        req_location_y = request.POST.get("location_y", None)

        # none require POST
        req_range = request.POST.get("location_range", 15)

        if not (req_animal or req_location_x or req_location_y):
            return Response("missing_field", status=status.HTTP_406_NOT_ACCEPTABLE)

        if req_animal not in dict(ANIMALS):
            return Response("animal_not_found", status=status.HTTP_404_NOT_FOUND)

        search = Search(user=user, animal=req_animal, range=int(req_range),
                    location=Point(float(req_location_x), float(req_location_y)))

        search.save()

        response = {
            "api_url": request.build_absolute_uri(reverse('a_search', args=(search.id,))),
            "id": search.id,
            "animal": search.animal,
            "user": {"username": search.user.username, "user_id": search.user.id},
            'location': [search.location.x, search.location.y],
            'range': search.range,
            "datetime": search.date.strftime('%d/%m/%Y %H:%M:%S')
        }

        return Response(response, status=status.HTTP_201_CREATED)

    def delete(self, request):
        id = request.POST.get("id", None)
        if not id:
            return Response("missing_field", status=status.HTTP_406_NOT_ACCEPTABLE)
        try:
            Search.objects.get(id=int(id)).delete()
        except Search.DoesNotExist:
            return Response("search_not_found", status=status.HTTP_404_NOT_FOUND)

        return Response(None, status=status.HTTP_200_OK)

class ASearchAPI(GenericAPIView):
    def get(self, request, search_id):
        try:
            search = Search.objects.get(id=int(search_id))
        except Search.DoesNotExist:
            return Response("search_not_found", status=status.HTTP_404_NOT_FOUND)

        response = {
            "api_url": request.build_absolute_uri(reverse('a_search', args=(search.id,))),
            "id": search.id,
            "animal": search.animal,
            "user": {"username": search.user.username, "user_id": search.user.id},
            'location': [search.location.x, search.location.y],
            'range': search.range,
            "datetime": search.date.strftime('%d/%m/%Y %H:%M:%S')
        }


        return Response(response, status=status.HTTP_200_OK)