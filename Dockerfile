FROM python:3

RUN apt-get update && apt-get install -y \
    binutils \
    libproj-dev \
    gdal-bin

ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt
COPY . /code/
